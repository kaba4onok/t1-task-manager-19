package ru.t1.rleonov.tm.command.task;

import ru.t1.rleonov.tm.util.TerminalUtil;

public final class TaskRemoveByIndex extends AbstractTaskCommand {

    public final static String COMMAND = "task-remove-by-index";

    public final static String DESCRIPTION = "Remove task by index.";

    @Override
    public void execute() {
        System.out.println("[REMOVE TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        getTaskService().removeByIndex(index);
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return COMMAND;
    }

}
