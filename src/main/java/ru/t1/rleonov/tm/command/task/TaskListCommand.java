package ru.t1.rleonov.tm.command.task;

import ru.t1.rleonov.tm.enumerated.Sort;
import ru.t1.rleonov.tm.model.Task;
import ru.t1.rleonov.tm.util.TerminalUtil;
import java.util.Arrays;
import java.util.List;

public final class TaskListCommand extends AbstractTaskCommand {

    public final static String COMMAND = "task-list";

    public final static String DESCRIPTION = "Show task list.";

    @Override
    public void execute() {
        System.out.println("[TASK LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(Sort.values()));
        final String sortType = TerminalUtil.nextLine();
        final Sort sort = Sort.toSort(sortType);
        final List<Task> tasks = getTaskService().findAll(sort);
        renderTasks(tasks);
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return COMMAND;
    }

}
