package ru.t1.rleonov.tm.exception.field;

import ru.t1.rleonov.tm.exception.user.AbstractUserException;

public final class RoleEmptyException extends AbstractUserException {

    public RoleEmptyException() {
        super("Error! Role is empty...");
    }

}
