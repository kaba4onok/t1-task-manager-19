package ru.t1.rleonov.tm.exception.field;

import ru.t1.rleonov.tm.exception.user.AbstractUserException;

public final class PasswordEmptyException extends AbstractUserException {

    public PasswordEmptyException() {
        super("Error! Password is empty...");
    }

}
