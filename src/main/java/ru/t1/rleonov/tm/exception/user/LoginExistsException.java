package ru.t1.rleonov.tm.exception.user;

public final class LoginExistsException extends AbstractUserException {

    public LoginExistsException() {
        super("Error! This login already exists...");
    }

}
