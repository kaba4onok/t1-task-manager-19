package ru.t1.rleonov.tm.repository;

import ru.t1.rleonov.tm.api.repository.IProjectRepository;
import ru.t1.rleonov.tm.model.Project;

public final class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    @Override
    public Project create(String name, String description) {
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        return add(project);
    }

    @Override
    public Project create(String name) {
        final Project project = new Project();
        project.setName(name);
        return add(project);
    }

}
