package ru.t1.rleonov.tm.api.service;

import ru.t1.rleonov.tm.enumerated.Status;
import ru.t1.rleonov.tm.model.Project;

public interface IProjectService extends IService<Project> {

    Project create(String name);

    Project create(String name, String description);

    Project updateById(String id, String name, String description);

    Project updateByIndex(Integer index, String name, String description);

    Project changeProjectStatusById(String id, Status status);

    Project changeProjectStatusByIndex(Integer index, Status status);

}
